/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready

document.addEventListener('deviceready', onDeviceReady, false);

const WEB = 'WEB';
const UPI = 'UPI';

const APP_ID = '--app-id-here--' // app-id-here
const APP_SECRET = '--app-secret-here--' // app-secret-here

const ENV = 'PROD' // 'TEST' or 'PROD'

function onDeviceReady() {
    // Cordova is now initialized. Have fun!
    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    let webElement = document.getElementById("onWEB");
    webElement.addEventListener('touchstart', (e) => addButtonClass(webElement));
    webElement.addEventListener('touchend', (e) => removeButtonClass(webElement));
    webElement.addEventListener("click", (e) => initiatePayment(WEB, null));

    let upiElement = document.getElementById("onUPI");
    upiElement.addEventListener('touchstart', (e) => addButtonClass(upiElement));
    upiElement.addEventListener('touchend', (e) => removeButtonClass(upiElement));
    upiElement.addEventListener("click", (e) => initiatePayment(UPI, null));

    getUpiApps()
}

function initiatePayment(mode, upiAppId) {

    let orderIdValue = "Order" + Math.floor(Math.random() * 100000) + 1;
    generateToken();

    function generateToken() {
        const options = {
            method: 'post',
            data: {
                orderId: orderIdValue,
                orderAmount: 1,
                orderCurrency: 'INR'
            },
            headers: {
                'Content-Type': 'application/json',
                'x-client-id': APP_ID,
                'x-client-secret': APP_SECRET
            }
        };

        let url = (ENV === 'TEST') ? 'https://test.cashfree.com/api/v2/cftoken/order' : 'https://api.cashfree.com/api/v2/cftoken/order';

        cordova.plugin.http.setDataSerializer("json");

        SpinnerDialog.show("Generating Token", null, null);
        cordova.plugin.http.sendRequest(url, options,
            (response) => {
                SpinnerDialog.hide();
                let result = JSON.parse(response.data);
                let params = getParamMap(result.cftoken)
                startPayment(params);
            }, (error) => {
                SpinnerDialog.hide();
                console.log('FROM Cordova : error = ' + error);
                console.log('FROM Cordova : error message = ' + error.message);
            })
    }

    function getParamMap(cftoken) {
        return {
            "appId": APP_ID,
            "orderId": orderIdValue,
            "orderAmount": "1",
            "orderNote": "Cashfree Test",
            "customerName": "Cashfree",
            "customerPhone": "9094395340",
            "customerEmail": "arjun@cashfree.com",
            "notifyUrl": "https://www.yourendpoint.com/",
            "orderCurrency": "INR",
            "stage": "PROD",
            "tokenData": cftoken
        }
    }

    function startPayment(params) {
        if (upiAppId !== null) {
            params.appName = upiAppId;
        }
        SpinnerDialog.show("Waiting for response", null, null);
        if (mode === UPI) {
            PgCordovaWrapper.startPaymentUPI(params, successFunction, failureFunction)
        } else {
            PgCordovaWrapper.startPaymentWEB(params, successFunction, failureFunction)
        }
    }

    function successFunction(result) {
        let output = '';
        let object = JSON.parse(result)
        Object.keys(object).forEach((key) => {
            if (key === 'signature') {
                const shortString = object[key].substring(0, 5).concat('...');
                output = output.concat(`${key} : ${shortString} <br>`);
            } else {
                output = output.concat(`${key} : ${object[key]} <br>`);
            }
        });
        document.getElementById('response_text').innerHTML = output;
        SpinnerDialog.hide();
    }

    function failureFunction(error) {
        console.log('FROM startPayment : error = ' + error);
        console.log('FROM startPayment : error message = ' + error.message);
        SpinnerDialog.hide();
    }
}

function addButtonClass(element) {
    element.classList.add('button-active');
}

function removeButtonClass(element) {
    element.classList.remove('button-active');
}

function getUpiApps() {
    SpinnerDialog.show("Fetching UPI Apps", null, null);
    PgCordovaWrapper.getUPIApps((response) => {
        handleUPIApps(response);
        SpinnerDialog.hide();
    }, (error) => {
        console.log('FROM startPayment : error = ' + error);
        console.log('FROM startPayment : error message = ' + error.message);
        SpinnerDialog.hide();
    })


    function getFormattedIcon(displayName, icon, id) {
        return `<div id="${id}" class="round_icon_buttons">
        <img class="upi_image" src="${icon}" alt="Upi App"/>
        <div class="upi_icons_text">${displayName}</div>
      </div>`;
    }

    function changeUPIArray(array) {
        document.getElementById('upi_icon_containers').innerHTML =
            array.reduce((prevValue, currentValue, index, arrays) => prevValue.concat(currentValue));
        const elements = document.getElementsByClassName('round_icon_buttons');
        for (let i = 0; i < elements.length; i++) {
            const element = elements.item(i);
            element.addEventListener('click', () => {
                initiatePayment(UPI, element.id);
            });
        }
    }

    function transFormUPIResponse(result) {
        const array = [];
        result.forEach(function (item) {
            array.push(new UPIApp(item.id, item.displayName, item.icon));
        }, this);
        return array;
    }

    function handleUPIApps(result) {
        const array = transFormUPIResponse(JSON.parse(result))
        const buttonArray = [];
        array.forEach((value => {
            const icon = getIcon(value.base64Icon);
            const button = getFormattedIcon(value.displayName, icon, value.id);
            buttonArray.push(button);
        }));
        if (array.length === 0) {
            buttonArray.push(`<div class="upi_app_not_found">No UPI Apps Found</div>`);
            changeUPIArray(buttonArray);
        }
        changeUPIArray(buttonArray);
    }

    function getIcon(base64String) {
        return `data:image/png;base64,${base64String}`;
    }

    class UPIApp {
        constructor(id, displayName, base64Icon) {
            this.id = id;
            this.displayName = displayName;
            this.base64Icon = base64Icon;
        }
    }

}
